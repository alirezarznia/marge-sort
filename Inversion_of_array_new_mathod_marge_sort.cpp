#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP(x , y) 			make_pair(x ,y)
#define 	MPP(x , y , z) 	    MP(x, MP(y,z))
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<long long ,pii>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
#define II ({int a; scanf("%d", &a); a;})
#define LL ({ll a; scanf("%lld", &a); a;})

typedef long long ll;

using namespace std;
vector<ll> vec;
int MA(ll l , ll m , ll r){
    ll sz = (r-l)+2; ll flo = l , flt =m+1 , fl =0 , cnt=0;
    ll tmp[sz];
    while(flo != m+1 && flt != r+1){
        if(vec[flo] <= vec[flt]) tmp[fl++] = vec[flo++];
        else    tmp[fl++]= vec[flt++] , cnt+=(m-flo)+1;
    }
    while(flo !=m+1)
        tmp[fl++]= vec[flo++];
    while(flt !=r+1)
        tmp[fl++]= vec[flt++];
    For(i , l , r+1)
        vec[i]=tmp[i-l];
    return cnt;
}

int MG(ll l , ll r){
    ll cnt =0;
    if(l<r){
    ll mid = (l+r)/2;
        cnt+=MG(l , mid);
        cnt+=MG(mid+1 , r);
        cnt+=MA(l , mid ,r);
    }
    return cnt;
}
int main(){
   //Test;
    ll t ;cin>>t;
    while(t--){
        vec.clear();
        ll n ;cin>>n;Rep(i , n){ll x ;cin>>x; vec.push_back(x);}
        cout<<MG(0 , vec.size()-1 )<<endl;
    }
}