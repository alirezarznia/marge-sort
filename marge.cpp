

/* The task is to complete merge() which is used
   in below mergeSort() */
/*
void mergeSort(int arr[], int l, int r) {
    if (l < r)   {
        int m = l+(r-l)/2;
        mergeSort(arr, l, m);
        mergeSort(arr, m+1, r);
        merge(arr, l, m, r);
    }
} 
*/
  void merge(int arr[], int l, int m, int r)
{
     long long no = (m-l)+1 , nt = (r-m);
     long long arro[no] , arrt[nt];
     for(int i = l ; i<=m ; i++)
        arro[i-l] = arr[i];
    for(int i = m+1 ; i<=r;i++)
        arrt[i-(m+1)] = arr[i];
    arro[no]=1e6 ; arrt[nt]=1e6;
    long long flo = 0 , flt = 0  , flk =l;
    while(flo != no || flt != nt){
        if(arro[flo] > arrt[flt])
            arr[flk++] = arrt[flt++];
        else
            arr[flk++] = arro[flo++];
    }
}
// Merges two subarrays of arr[].  First subarray is arr[l..m]
// Second subarray is arr[m+1..r]
